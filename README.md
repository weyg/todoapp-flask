# TodoApp with Flask

For development, start with `FLASK_ENV=development python run.py` or set environment variable in some other way.

Generate migrations with `FLASK_ENV=development FLASK_APP=application flask db migrate -m "<migration name>"` in root directory, edit generated migrations as needed before commit. See also [this](https://alembic.sqlalchemy.org/en/latest/batch.html) for sqlite.

Run tests with `FLASK_ENV=testing coverage run -m pytest -v --assert=plain` and see coverage with `coverage report`, or generate html report with `coverage html`.

For Heroku you need to set SECRET_KEY environment variable to some (preferably safe) value, or add `--workers=1` to Procfile.
