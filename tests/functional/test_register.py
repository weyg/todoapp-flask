from time import time_ns

def test_registerform(client):
    response = client.get('/register')

    assert response.status_code == 200
    assert 'username' in response.data.decode('utf-8')
    assert 'password' in response.data.decode('utf-8')
    assert 'email' in response.data.decode('utf-8')
    assert 'value="Register"' in response.data.decode('utf-8')

def test_emptyregisterpost(client):
    data = {
        'username': '',
        'email': '',
        'password': ''
    }
    response = client.post('/register', data=data)

    assert response.status_code == 200
    assert 'This field is required' in response.data.decode('utf-8')

def test_shortregisterpost(client):
    data = {
        'username': 'a',
        'email': 'test@test.invalid',
        'password': 'a',
        'password_confirm': 'a'
    }
    response = client.post('/register', data=data)

    assert response.status_code == 200
    assert 'Field must be' in response.data.decode('utf-8')

def test_register_pw_match(client):
    data = {
        'username': 'testuser',
        'email': 'test@test.invalid',
        'password': 'testpw',
        'password_confirm': 'pwtest'
    }
    response = client.post('/register', data=data)

    assert response.status_code == 200
    assert 'do not match' in response.data.decode('utf-8')

def test_register(client):
    t = str(time_ns())
    data = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    response = client.post('/register', data=data, follow_redirects=True)

    assert response.status_code == 200
    assert 'Registration successful' in response.data.decode('utf-8')

def test_register_twice(client):
    t = str(time_ns())
    data = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    response = client.post('/register', data=data)
    response = client.post('/register', data=data)

    assert response.status_code == 200
    assert 'already in use' in response.data.decode('utf-8')
