def test_index(client):
    response = client.get('/')

    assert response.status_code == 200
    assert 'TodoApp' in response.data.decode('utf-8')

def test_index_post(client):
    response = client.post('/')

    assert response.status_code == 405
    assert 'TodoApp' not in response.data.decode('utf-8')
