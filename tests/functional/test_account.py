def test_account_details(client):
    rdata = {
        'username': 'testuser',
        'email': 'test@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser',
        'email': 'test@test.invalid',
        'password': 'testpw'
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)
    response = client.get('/account')

    assert response.status_code == 200
    assert 'testuser' in response.data.decode('utf-8')
    assert 'test@test.invalid' in response.data.decode('utf-8')

def test_account_details_login_required(client):
    response = client.get('/account', follow_redirects=True)

    assert response.status_code == 200
    assert 'Please log in' in response.data.decode('utf-8')
    assert 'value="Login"' in response.data.decode('utf-8')
