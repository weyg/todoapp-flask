from time import time_ns

def test_add_task_form(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw'
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)
    response = client.get('/tasks/add')

    assert response.status_code == 200
    assert 'Task name' in response.data.decode('utf-8')
    assert 'description' in response.data.decode('utf-8')
    assert 'value="Add task"' in response.data.decode('utf-8')

def test_add_task(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw'
    }
    tdata = {
        'taskname': 'task' + t,
        'description': 'blablabla bla blabla blablabla',
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)
    response = client.post('/tasks/add', data=tdata, follow_redirects=True)

    assert response.status_code == 200
    assert 'Task added' in response.data.decode('utf-8')

def test_add_task_form_login_required(client):
    response = client.get('/tasks/add', follow_redirects=True)

    assert response.status_code == 200
    assert 'Please log in' in response.data.decode('utf-8')
    assert 'value="Login"' in response.data.decode('utf-8')

def test_list_tasks(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw'
    }
    tdata = {
        'taskname': 'task' + t,
        'description': 'blablabla bla blabla blablabla ' + t,
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)
    response = client.post('/tasks/add', data=tdata)
    t2 = str(time_ns())
    tdata = {
        'taskname': 'task' + t2,
        'description': 'blablabla bla blabla blablabla ' + t2,
    }
    response = client.post('/tasks/add', data=tdata)
    response = client.get('/tasks')

    assert response.status_code == 200
    assert 'task' + t in response.data.decode('utf-8')
    assert 'task' + t2 in response.data.decode('utf-8')
