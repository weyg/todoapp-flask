from time import time_ns

def test_loginform(client):
    response = client.get('/login')

    assert response.status_code == 200
    assert 'username' in response.data.decode('utf-8')
    assert 'password' in response.data.decode('utf-8')
    assert 'value="Login"' in response.data.decode('utf-8')

def test_emptyloginpost(client):
    data = {
        'username': '',
        'password': ''
    }
    response = client.post('/login', data=data)

    assert response.status_code == 200
    assert 'This field is required' in response.data.decode('utf-8')

def test_shortloginpost(client):
    data = {
        'username': 'a',
        'password': 'a'
    }
    response = client.post('/login', data=data)

    assert response.status_code == 200
    assert 'Field must be' in response.data.decode('utf-8')

def test_register_login_fail(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'pwtest'
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)

    assert response.status_code == 200
    assert 'No such user name or password' in response.data.decode('utf-8')

def test_register_login_success(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw'
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata, follow_redirects=True)

    assert response.status_code == 200
    assert 'login successful' in response.data.decode('utf-8')
    assert 'value="Logout"' in response.data.decode('utf-8')
    assert 'Hi testuser' in response.data.decode('utf-8')

def test_logout(client):
    t = str(time_ns())
    rdata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw',
        'password_confirm': 'testpw'
    }
    ldata = {
        'username': 'testuser' + t,
        'email': 'test' + t + '@test.invalid',
        'password': 'testpw'
    }
    response = client.post('/register', data=rdata)
    response = client.post('/login', data=ldata)
    response = client.get('/account')

    assert response.status_code == 200
    assert 'testuser' in response.data.decode('utf-8')
    assert 'test' + t + '@test.invalid' in response.data.decode('utf-8')

    response = client.post('/logout', data={}, follow_redirects=True)

    assert response.status_code == 200
    assert 'logout successful' in response.data.decode('utf-8')
    assert 'testuser' not in response.data.decode('utf-8')
