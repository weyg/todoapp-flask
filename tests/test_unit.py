
def test_new_user(new_user):
    assert new_user.username == 'testuser'
    assert new_user.email == 'test@abc.invalid'
    assert new_user.password == 'testpw'
    assert new_user.admin == False
    assert new_user.deleted == False

def test_new_task(new_task):
    assert new_task.taskname == 'taskname'
    assert new_task.description == 'description'
    assert new_task.owner_id == 1
    assert new_task.important == False
    assert new_task.taskdone == False
