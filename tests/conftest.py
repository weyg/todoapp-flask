import pytest
from application import app
from application.user.models import User
from application.task.models import Task

@pytest.fixture(scope='module')
def new_user():
    user = User('testuser', 'test@abc.invalid', 'testpw')
    return user

@pytest.fixture(scope='module')
def new_task():
    task = Task('taskname', 'description', 1)
    return task

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['WTF_CSRF_ENABLED'] = False
    # with csrf would be better, but this way for now
    client = app.test_client()

    yield client
