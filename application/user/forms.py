from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, EmailField
from wtforms.validators import DataRequired, Email, Length, EqualTo

class RegisterForm(FlaskForm):
    username = StringField('User name', validators=[DataRequired(),
                                Length(min=3, max=140)])
    email = EmailField('Email address', validators=[DataRequired(),
                                Email(), Length(min=3, max=140)])
    password = PasswordField('Password', validators=[DataRequired(),
                            Length(min=3, max=140), EqualTo('password_confirm',
                                                    message='passwords do not match')])
    password_confirm = PasswordField('Password again', validators=[DataRequired(),
                                                        Length(min=3, max=140)])
