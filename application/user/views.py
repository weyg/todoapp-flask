from flask import Blueprint, redirect, render_template, request, flash, url_for
from werkzeug.security import generate_password_hash
from flask_login import login_required

from application import db
from application.user.forms import RegisterForm
from application.user.models import User

user_bp = Blueprint('user', __name__)

@user_bp.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if form.validate_on_submit():
        try:
            user = User(form.username.data, form.email.data,
                        generate_password_hash(form.password.data))
            db.session.add(user)
            db.session.commit()
            flash('Registration successful!', 'success')
            return redirect(url_for('index.index'))
        except:
            flash('account name or email already in use', 'danger')
            return render_template("user/register.html", form = form)
    return render_template("user/register.html", form = form)

@user_bp.get('/account')
@login_required
def account():
    return render_template("user/account.html")
