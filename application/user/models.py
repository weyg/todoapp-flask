from flask_login import UserMixin
from application import db

class User(db.Model, UserMixin):
    __tablename__ = "account"

    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
    onupdate=db.func.current_timestamp())

    tasks = db.relationship('Task', order_by='desc(Task.important)', backref='account', lazy=True)

    username = db.Column(db.String(144), nullable=False, unique=True)
    email = db.Column(db.String(144), nullable=False, unique=True)
    password = db.Column(db.String(144), nullable=False)
    admin = db.Column(db.Boolean, nullable=False)
    deleted = db.Column(db.Boolean, nullable=False)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = password
        self.admin = False
        self.deleted = False
