from flask import Flask

app = Flask(__name__)

app.config.from_prefixed_env()
if app.config['ENV'] == 'development':
    app.config.from_object('application.config-dev')
elif app.config['ENV'] == 'testing':
    app.config.from_object('application.config-test')
else:
    app.config.from_object('application.config')

from application.db import db, migrate, db_upgrade
from application.auth import login_manager, register_emptyform
from application.views import main_page
from application.auth.views import auth_bp
from application.user.views import user_bp
from application.task.views import task_bp

db.init_app(app)
migrate.init_app(app, db)

login_manager.init_app(app)
register_emptyform(app)

app.register_blueprint(main_page)
app.register_blueprint(auth_bp)
app.register_blueprint(user_bp)
app.register_blueprint(task_bp)

db_upgrade(app)
