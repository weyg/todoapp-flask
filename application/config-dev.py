from os import environ
from secrets import token_urlsafe

SECRET_KEY = environ.get('SECRET_KEY', default=token_urlsafe(16))
SQLALCHEMY_DATABASE_URI = 'sqlite:///todo.db'
SQLALCHEMY_ECHO = True
