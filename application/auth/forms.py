from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Length

class LoginForm(FlaskForm):
    username = StringField('User name', validators=[DataRequired(), Length(min=3, max=140)])
    password = PasswordField('Password', validators=[DataRequired(), Length(min=3, max=140)])
