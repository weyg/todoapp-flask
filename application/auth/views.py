from flask import Blueprint, redirect, render_template, request, flash, url_for
from werkzeug.security import check_password_hash
from flask_login import login_user, login_required, logout_user
from flask_wtf import FlaskForm

from application.auth.forms import LoginForm
from application.user.models import User

auth_bp = Blueprint('auth', __name__)

@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if not user or not check_password_hash(user.password, form.password.data):
            flash('No such user name or password', 'danger')
            return render_template("auth/login.html", form=form)
        login_user(user)
        flash('login successful', 'success')
        return redirect(url_for('index.index'))
    return render_template("auth/login.html", form=form)

@auth_bp.route('/logout', methods=['POST'])
@login_required
def logout():
    form = FlaskForm(request.form)
    if form.validate_on_submit():
        logout_user()
        flash('logout successful', 'success')
        return redirect(url_for('index.index'))
    flash('invalid logout attempt', 'danger')
    return redirect(url_for('index.index'))
