from flask_login import LoginManager
from flask_wtf import FlaskForm
from application.user.models import User

login_manager = LoginManager()
login_manager.login_view = 'auth.login'

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

def inject_emptyform():
    return dict(emptyform=FlaskForm())

def register_emptyform(app):
    app.context_processor(inject_emptyform)
