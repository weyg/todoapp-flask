from os import environ
from secrets import token_urlsafe

SECRET_KEY = environ.get('SECRET_KEY', default=token_urlsafe(16))
uri = environ.get('DATABASE_URL')
if uri.startswith("postgres://"):
    uri = uri.replace("postgres://", "postgresql://", 1)
SQLALCHEMY_DATABASE_URI = uri
SQLALCHEMY_ENGINE_OPTIONS = {"pool_recycle": 600}
