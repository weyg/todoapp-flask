from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired, Length

class TaskForm(FlaskForm):
    taskname = StringField('Task name', validators=[DataRequired(), Length(min=3, max=140)])
    description = TextAreaField('Description', validators=[Length(max=1900)])
