from flask import Blueprint, redirect, render_template, request, flash, url_for
from flask_login import login_required, current_user
from flask_wtf import FlaskForm

from application import db
from application.task.forms import TaskForm
from application.task.models import Task

task_bp = Blueprint('task', __name__)

@task_bp.get('/tasks')
@login_required
def list_tasks():
    return render_template("task/list_tasks.html")

@task_bp.route('/tasks/add', methods=['GET', 'POST'])
@login_required
def create_task():
    form = TaskForm(request.form)
    if form.validate_on_submit():
        task = Task(form.taskname.data, form.description.data,
                    current_user.id)
        db.session.add(task)
        db.session.commit()
        flash('Task added!', 'success')
        return redirect(url_for('task.list_tasks'))
    return render_template("task/add_task.html", form = form)

@task_bp.get('/tasks/<int:task_id>')
@login_required
def show_task(task_id):
    task = Task.query.get(task_id)
    if not task or task.owner_id != current_user.id:
        return redirect(url_for('task.list_tasks'))
    return render_template("task/single_task.html", task=task)

@task_bp.post('/tasks/<int:task_id>/done')
@login_required
def task_done(task_id):
    form = FlaskForm(request.form)
    task = Task.query.get(task_id)
    if not task or task.owner_id != current_user.id:
        return redirect(url_for('task.list_tasks'))
    if form.validate_on_submit():
        task.taskdone = not task.taskdone
        db.session.commit()
    return redirect(url_for('task.show_task', task_id=task_id))

@task_bp.post('/tasks/<int:task_id>/important')
@login_required
def task_important(task_id):
    form = FlaskForm(request.form)
    task = Task.query.get(task_id)
    if not task or task.owner_id != current_user.id:
        return redirect(url_for('task.list_tasks'))
    if form.validate_on_submit():
        task.important = not task.important
        db.session.commit()
    return redirect(url_for('task.show_task', task_id=task_id))
