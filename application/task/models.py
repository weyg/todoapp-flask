from application import db

class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
    onupdate=db.func.current_timestamp())

    owner_id = db.Column(db.Integer, db.ForeignKey('account.id'), nullable=False)

    taskname = db.Column(db.String(144), nullable=False)
    description = db.Column(db.String(2000))
    important = db.Column(db.Boolean, nullable=False)
    taskdone = db.Column(db.Boolean, nullable=False)

    def __init__(self, taskname, description, owner_id):
        self.taskname = taskname
        self.description = description
        self.owner_id = owner_id
        self.important = False
        self.taskdone = False
