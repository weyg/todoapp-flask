"""rename done to taskdone

Revision ID: 7f73b1340b77
Revises: 00a69fc9e2f3
Create Date: 2022-02-15 20:51:40.146668

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7f73b1340b77'
down_revision = '00a69fc9e2f3'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('task', column_name='done', new_column_name='taskdone')


def downgrade():
    op.alter_column('task', column_name='taskdone', new_column_name='done')
