"""add deleted flag

Revision ID: 00dd380c7dbc
Revises: 194cef4abc4e
Create Date: 2022-02-14 14:09:33.721363

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '00dd380c7dbc'
down_revision = '194cef4abc4e'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('account', sa.Column('deleted', sa.Boolean(), nullable=True))
    op.execute(sa.table('account', sa.column('deleted')).update().values(deleted=False))
    with op.batch_alter_table('account') as batch_op:
        batch_op.alter_column('deleted', nullable=False)


def downgrade():
    with op.batch_alter_table('account') as batch_op:
        batch_op.drop_column('deleted')
