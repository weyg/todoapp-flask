"""add task

Revision ID: 00a69fc9e2f3
Revises: 00dd380c7dbc
Create Date: 2022-02-15 12:25:30.945178

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '00a69fc9e2f3'
down_revision = '00dd380c7dbc'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('task',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('date_created', sa.DateTime(), nullable=True),
    sa.Column('date_modified', sa.DateTime(), nullable=True),
    sa.Column('owner_id', sa.Integer(), nullable=False),
    sa.Column('taskname', sa.String(length=144), nullable=False),
    sa.Column('description', sa.String(length=2000)),
    sa.Column('important', sa.Boolean(), nullable=False),
    sa.Column('done', sa.Boolean(), nullable=False),
    sa.ForeignKeyConstraint(['owner_id'], ['account.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('task')
    # ### end Alembic commands ###
