"""rename account name to username

Revision ID: 194cef4abc4e
Revises: 8ef5d8c35b3d
Create Date: 2022-02-14 13:19:03.750070

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '194cef4abc4e'
down_revision = '8ef5d8c35b3d'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('account', 'accountname', new_column_name='username')


def downgrade():
    op.alter_column('account', 'username', new_column_name='accountname')
